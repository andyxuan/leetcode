class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        length = len(nums)
        if length < 2:
            return []

        for i in range(0, length):
            for j in range(i+1, length):
                if nums[i] + nums[j] == target:
                    return [i, j]
        return []
        



if __name__ == '__main__':
    s = Solution()
    print(s.twoSum([2,7,11,15], 9))
    