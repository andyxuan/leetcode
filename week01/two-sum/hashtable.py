class Solution:
    def twoSum(self, nums, target):
        if len(nums) < 2:
            return []
        
        hashtable = {}
        for index, num in enumerate(nums):
            if target-num in hashtable:
                return [hashtable[target-num], index]
            hashtable[num] = index

        return []
        



if __name__ == '__main__':
    s = Solution()
    print(s.twoSum([2,7,11,15], 9))