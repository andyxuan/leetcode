class Solution:
    def findCircleNum(self, isConnected):
        if not isConnected or not isConnected[0]:
             return 0
        
        length = len(isConnected)
        visited, result = [False] * length, 0

        def _dfs(index):
            visited[index] = True

            for j in range(length):
                if isConnected[index][j] == 1 and not visited[j]:
                    _dfs(j)
        


        for i in range(length):
            if not visited[i]:
                result += 1
                _dfs(i)

        return result



if __name__ == '__main__':
    isConnected = [[1,1,0],[1,1,0],[0,0,1]]
    print(Solution().findCircleNum(isConnected))
    