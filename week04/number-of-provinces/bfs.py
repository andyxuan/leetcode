import collections

class Solution:
    def findCircleNum(self, isConnected):
        if not isConnected or not isConnected[0]:
             return 0
        
        length = len(isConnected)
        visited, result = [False] * length, 0

        for i in range(length):
            if not visited[i]:
                Q = collections.deque([i])
                while Q:
                    j = Q.popleft()
                    visited[j] = True
                    for k in range(length):
                        if isConnected[j][k] == 1 and not visited[k]:
                            Q.append(k)
                result += 1


        return result



if __name__ == '__main__':
    isConnected = [[1,1,0],[1,1,0],[0,0,1]]
    print(Solution().findCircleNum(isConnected))
    