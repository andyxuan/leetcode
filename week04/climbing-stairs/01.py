class Solution:
    def __init__(self):
        self.__mapper = {1: 1, 2: 2}

    def climbStairs(self, n: int) -> int:
        if self.__mapper.get(n, None):
            return self.__mapper[n]
        result = self.climbStairs(n-1) + self.climbStairs(n-2)
        self.__mapper[n] = result
        
        return result