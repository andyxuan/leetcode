import math
class Solution:
    def climbStairs(self, n: int) -> int:
        sqrt5 = 5**0.5
        return int((math.pow((1+sqrt5)/2, n+1) - math.pow((1-sqrt5)/2, n+1))/sqrt5)