class Solution:
    def maxProfit(self, prices) -> int:
        perfect = 0
        for i in range(1, len(prices)):
            tmp = prices[i] - prices[i-1]
            if tmp > 0: perfect += tmp

        return perfect

if __name__ == '__main__':
    s = Solution()
    print(s.maxProfit([7,1,5,3,6,4]))
    