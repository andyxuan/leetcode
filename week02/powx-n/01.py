class Solution:

    def myPow(self, x: float, n: int) -> float:
        def _mult(N):
            if N == 0:
                return 1.0
                
            y = _mult(N // 2)
            return y * y if N % 2 == 0 else y * y * x
        
        return _mult(n) if n >= 0 else 1.0/_mult(-n)

if __name__ == '__main__':
    s = Solution()
    print(s.myPow(2.00000, 10))
    