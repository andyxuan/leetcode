class Node:
    def __init__(self, key=0):
        self.key = key
        self.value = 0
        self.prev = None
        self.next = None

class LRUCache:
    def __init__(self, capacity: int):
        super().__init__()
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head
        self.capacity = capacity
        self.cache = dict()

    def get(self, key: int) -> int:
        if key not in self.cache.keys():
            return -1
        node = self.cache[key]
        self.__move_to_tail(node)
        return node.value

    def put(self, key: int, value: int) -> None:
        node = None
        if key in self.cache.keys():
            node = self.cache[key]
            self.__move_to_tail(node)
        else:
            node = Node(key)
            self.__add_to_tail(node)
            self.cache[key] = node
        node.value = value
        if len(self.cache) > self.capacity:
            removed = self.__remove_head()
            self.cache.pop(removed.key)

    def __move_to_tail(self, node: Node) -> None:
        self.__remove_node(node)
        self.__add_to_tail(node)

    def __remove_node(self, node: Node) -> None:
        node.prev.next = node.next
        node.next.prev = node.prev

    def __add_to_tail(self, node: Node) -> None:
        node.prev = self.tail.prev
        node.next = self.tail
        self.tail.prev.next = node
        self.tail.prev = node
    
    def __remove_head(self) -> Node:
        node = self.head.next
        self.__remove_node(node)
        return node




# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)