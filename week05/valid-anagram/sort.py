'''
排序法
'''
class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if not s or not t or len(s) != len(t):
            return False
        
        arr1 = sorted(list(s))
        arr2 = sorted(list(t))
        
        for i in range(len(arr1)):
            if arr1[i] != arr2[i]: return False

        return True
            

        

if __name__ == '__main__':
    s = Solution()
    print(s.isAnagram("anagram", "nagaram"))
    print(s.isAnagram("rat", "car"))
    