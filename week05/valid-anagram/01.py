class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if not s or not t or len(s) != len(t):
            return False

        res = {}
        for c in s:
            res[c] = res[c] + 1 if c in res else 1
        
        for c in t:
            if c not in res or res[c] <= 0:
                return False
            res[c] -= 1

        
        return True
            

        

if __name__ == '__main__':
    s = Solution()
    print(s.isAnagram("anagram", "nagaram"))
    print(s.isAnagram("rat", "car"))
    