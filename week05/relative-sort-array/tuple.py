class Solution:
    def relativeSortArray(self, arr1, arr2):
        if not arr1 or not arr2:
            return []

        length = len(arr2)
        rank = {x: i for i, x in enumerate(arr2)}
        arr1.sort(key=lambda x: (0, rank[x]) if x in rank else (1, x))
        return arr1
        


if __name__ == '__main__':
    s = Solution()
    arr1, arr2 = [2,3,1,3,2,4,6,7,9,2,19], [2,1,4,3,9,6]
    print(s.relativeSortArray(arr1, arr2))
    